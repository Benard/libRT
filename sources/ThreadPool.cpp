//
// Created by benard_g on 10/09/2017.
//

#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <vector>
#include <queue>
#include "rt/ThreadPool.hpp"

/*
 * Impl
 */
struct  rt::ThreadPool::Impl {
public:
    std::vector<std::thread>    workers;

public:
    std::atomic<std::size_t>    nb_working;     /*!< The number of thread currently working */
    std::atomic<bool>           end_tasks;      /*!< Indicate if the thread need to stop working or not */
    std::atomic<bool>           joined;         /*!< true if the pool has already been joined */

public:
    std::mutex                  mutex;
    std::condition_variable     cond_var;

public:
    std::queue<std::function<void()>>   task_queue;

public:
    Impl();
};

rt::ThreadPool::Impl::Impl():
    workers(),
    nb_working(0),
    end_tasks(false),
    joined(false),
    mutex(),
    cond_var(),
    task_queue()
{}

/*
 * ThreadPool
 */
rt::ThreadPool::ThreadPool(std::size_t nb_thread):
    _pimpl(std::make_unique<ThreadPool::Impl>())
{
    (*_pimpl).workers.reserve(nb_thread);

    for (std::size_t i = 0; i < nb_thread; ++i) {
        (*_pimpl).workers.emplace_back(std::thread([this]() {
            std::unique_lock<decltype((*_pimpl).mutex)>     lock((*_pimpl).mutex);

            while (true) {
                // Wait while the task_queue is empty
                while ((*_pimpl).task_queue.empty() && !(*_pimpl).end_tasks)
                    (*_pimpl).cond_var.wait(lock);
                if ((*_pimpl).task_queue.empty() && (*_pimpl).end_tasks)
                    return ;

                // Get the next task to do
                auto task = (*_pimpl).task_queue.front();
                (*_pimpl).task_queue.pop();
                lock.unlock();
                (*_pimpl).cond_var.notify_one();

                // Execute the task
                ++(*_pimpl).nb_working;
                task();
                --(*_pimpl).nb_working;

                lock.lock();
            }
        }));
    }
}

rt::ThreadPool::~ThreadPool() noexcept {
    join();
}

rt::ThreadPool::ThreadPool(rt::ThreadPool &&) noexcept = default;
rt::ThreadPool  &rt::ThreadPool::operator=(rt::ThreadPool &&) noexcept = default;

void rt::ThreadPool::addTask(std::function<void()> const &task) {
    std::unique_lock<decltype((*_pimpl).mutex)>     lock((*_pimpl).mutex);

    (*_pimpl).task_queue.push(task);
    lock.unlock();
    (*_pimpl).cond_var.notify_one();
}

std::size_t     rt::ThreadPool::nbThreads() const noexcept {
    return (*_pimpl).workers.size();
}

std::size_t     rt::ThreadPool::nbWorkingThreads() const noexcept {
    return (*_pimpl).nb_working;
}

std::size_t     rt::ThreadPool::nbAvailableThreads() const noexcept {
    return (*_pimpl).workers.size() - (*_pimpl).nb_working;
}

void    rt::ThreadPool::join() noexcept {
    if (!(*_pimpl).joined) {
        (*_pimpl).end_tasks = true;
        (*_pimpl).cond_var.notify_all();
        for (auto &thread : (*_pimpl).workers)
            thread.join();
        (*_pimpl).joined = true;
    }
}
