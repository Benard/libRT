//
// Created by droid on 10/22/17.
//

#include <cstring>
#include <cerrno>
#include <stdexcept>
#include "rt/signal/Manager.hpp"
#include "rt/strerror.hpp"


rt::signal::Manager::Manager():
    _id(1),
    _call_backs(),
    _ignored_codes()
{}

std::size_t rt::signal::Manager::handle(rt::signal::Code signal_code, Handler handler) {
    if (!isHandled(signal_code))
        assignHandler(signal_code, &rt::signal::Manager::sigHandler);

    _call_backs.insert(std::make_pair(_id, std::make_pair(signal_code, handler)));
    removeFromIgnored(signal_code);
    return _id++;
}

void rt::signal::Manager::ignore(rt::signal::Code signal_code) {
    assignHandler(signal_code, SIG_IGN);
    removeFromHandled(signal_code);
    _ignored_codes.emplace(signal_code);
}

void rt::signal::Manager::reset(rt::signal::Code signal_code) {
    assignHandler(signal_code, SIG_DFL);
    removeFromHandled(signal_code);
    removeFromIgnored(signal_code);
}

void rt::signal::Manager::reset(std::size_t handler_id) {
    auto it = _call_backs.find(handler_id);

    if (it != _call_backs.end()) {
        auto signal_code = (*it).second.first;
        _call_backs.erase(it);

        if (!isHandled(signal_code))
            assignHandler(signal_code, SIG_DFL);
    }
}

void rt::signal::Manager::reset() {
    std::set<signal::Code>     codes;

    auto it = _call_backs.begin();
    while (it != _call_backs.end())
        codes.emplace((*it).second.first);
    _call_backs.clear();

    for (auto code : codes)
        assignHandler(code, SIG_DFL);
    for (auto code : _ignored_codes)
        assignHandler(code, SIG_DFL);
    _ignored_codes.clear();
}

bool rt::signal::Manager::isHandled(rt::signal::Code signal_code) const noexcept {
    for (auto const &pair : _call_backs) {
        if (pair.second.first == signal_code)
            return true;
    }
    return false;
}

bool rt::signal::Manager::isIgnored(rt::signal::Code signal_code) const noexcept {
    return _ignored_codes.find(signal_code) != _ignored_codes.end();
}


void rt::signal::Manager::sigHandler(int sig_value) {
    auto &signal_manager = Manager::get();

    for (auto const &call_back : signal_manager._call_backs) {
        if (static_cast<int>(call_back.second.first) == sig_value)
            call_back.second.second();
    }
}

void rt::signal::Manager::assignHandler(rt::signal::Code signal_code, void (*handler)(int)) {
    if (Code::UNSUPPORTED == signal_code)
        throw std::invalid_argument("Trying to handle signal with UNSUPPORTED type");

    auto ret = ::signal(static_cast<int>(signal_code), handler);

    if (SIG_ERR == ret)
        throw std::runtime_error(rt::strerror(errno));
}

void rt::signal::Manager::removeFromIgnored(rt::signal::Code signal_code) {
    auto it = _ignored_codes.find(signal_code);

    if (it != _ignored_codes.end())
        _ignored_codes.erase(it);
}

void rt::signal::Manager::removeFromHandled(rt::signal::Code signal_code) {
    auto it = _call_backs.begin();
    while (it != _call_backs.end()) {
        if ((*it).second.first == signal_code)
            it = _call_backs.erase(it);
        else
            ++it;
    }
}
