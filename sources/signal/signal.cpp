//
// Created by droid on 10/24/17.
//

#include "rt/signal/signal.hpp"

rt::signal::Token   rt::signal::handle(signal::Code signal_code, Manager::Handler handler) {
    std::size_t     id = rt::signal::Manager::get().handle(signal_code, handler);
    return rt::signal::Token(id);
}
