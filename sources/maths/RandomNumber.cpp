//
// Created by artoo on 11/5/17.
//

#include <random>
#include "rt/maths/RandomNumber.hpp"

//
// Impl
//
struct   rt::maths::RandomNumber::Impl {
public:
    std::random_device  seed;
    std::mt19937_64     generator;

public:
    using Dist = std::uniform_int_distribution<std::size_t>;
    std::unique_ptr<Dist>   distribution_ptr;

public:
    Impl();
    Impl(std::size_t min_bound, std::size_t max_bound);
};

rt::maths::RandomNumber::Impl::Impl():
    seed(),
    generator(seed()),
    distribution_ptr(std::make_unique<Dist>())
{}

rt::maths::RandomNumber::Impl::Impl(std::size_t min_bound, std::size_t max_bound):
    seed(),
    generator(seed()),
    distribution_ptr(std::make_unique<Dist>(min_bound, max_bound))
{}

//
// RandomNumber
//
rt::maths::RandomNumber::RandomNumber():
    _pimpl(std::make_unique<Impl>())
{}

rt::maths::RandomNumber::RandomNumber(std::size_t min_bound, std::size_t max_bound):
    _pimpl(std::make_unique<Impl>(min_bound, max_bound))
{}

rt::maths::RandomNumber::~RandomNumber() noexcept = default;

rt::maths::RandomNumber::RandomNumber(RandomNumber &&) noexcept = default;
rt::maths::RandomNumber &rt::maths::RandomNumber::operator=(RandomNumber &&) noexcept = default;


std::size_t rt::maths::RandomNumber::rand() {
    return (*_pimpl->distribution_ptr)(_pimpl->generator);
}

void rt::maths::RandomNumber::setBoundaries(std::size_t min_bound, std::size_t max_bound) {
    _pimpl->distribution_ptr = std::make_unique<Impl::Dist>(min_bound, max_bound);
}

std::size_t rt::maths::RandomNumber::min() const noexcept {
    return _pimpl->distribution_ptr->min();
}

std::size_t rt::maths::RandomNumber::max() const noexcept {
    return _pimpl->distribution_ptr->max();
}
