//
// Created by benard_g on 10/13/2017.
//

#include <windows.h>
#include <stdexcept>
#include "rt/plugin/Library.hpp"

/*
 * Impl
 */
using HandlePtr = std::unique_ptr<std::remove_pointer_t<HINSTANCE>, void (*)(HINSTANCE)>;

struct  rt::plugin::Library::Impl {
public:
    std::string     library_name;

public:
    HandlePtr       handle;

public:
    explicit Impl(std::string const &_library_name);
};

rt::plugin::Library::Impl::Impl(std::string const &_library_name):
        library_name(_library_name),
        handle(nullptr, [](HINSTANCE handle_ptr) { if (nullptr != handle_ptr) ::FreeLibrary(handle_ptr); })
{}


/*
 * Library
 */
rt::plugin::Library::Library(std::string const &library_pathname):
    _pimpl(std::make_unique<Impl>(library_pathname))
{
    _pimpl->handle.reset(::LoadLibrary(_pimpl->library_name.c_str()));
    if (nullptr == _pimpl->handle)
        throw std::runtime_error("Could not load library: \"" + _pimpl->library_name + "\"");
}


void        *rt::plugin::Library::loadSymbol(std::string const &symbol_name) {
    void    *sym = ::GetProcAddress(_pimpl->handle.get(), symbol_name.c_str());

    if (nullptr == sym)
        throw std::runtime_error("Could not load symbol: \"" + symbol_name + "\"");
    return sym;
}


std::string const &rt::plugin::Library::name() const noexcept {
    return _pimpl->library_name;
}
