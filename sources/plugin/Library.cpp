//
// Created by droid on 10/11/17.
//

#include "rt/platform.hpp"
#include "rt/plugin/Library.hpp"

#ifdef RT_WIN_OS
    #include "Library_Win.impl.cpp"
#else
    #include "Library_Unix.impl.cpp"
#endif


rt::plugin::Library::~Library() noexcept = default;

rt::plugin::Library::Library(rt::plugin::Library &&) noexcept = default;
rt::plugin::Library    &rt::plugin::Library::operator=(rt::plugin::Library &&) noexcept = default;
