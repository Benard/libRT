//
// Created by droid on 10/11/17.
//

#include <dlfcn.h>
#include <stdexcept>
#include "rt/plugin/Library.hpp"

/*
 * Impl
 */
using HandlePtr = std::unique_ptr<void, void (*)(void *)>;

struct  rt::plugin::Library::Impl {
public:
    std::string     library_name;

public:
    HandlePtr       handle;

public:
    explicit Impl(std::string const &_library_name);
};


rt::plugin::Library::Impl::Impl(std::string const &_library_name):
    library_name(_library_name),
    handle(nullptr, [](void *handle_ptr) { if (nullptr != handle_ptr) ::dlclose(handle_ptr); })
{}


/*
 * Library
 */
rt::plugin::Library::Library(std::string const &library_pathname):
    _pimpl(std::make_unique<Impl>(library_pathname))
{
    _pimpl->handle.reset(::dlopen(library_pathname.c_str(), RTLD_NOW | RTLD_GLOBAL));
    if (!_pimpl->handle)
        throw std::runtime_error(::dlerror());
}


void    *rt::plugin::Library::loadSymbol(std::string const &symbol_name) {
    void    *sym = ::dlsym(_pimpl->handle.get(), symbol_name.c_str());
    if (!sym)
        throw std::runtime_error(::dlerror());
    return sym;
}


std::string const &rt::plugin::Library::name() const noexcept {
    return _pimpl->library_name;
}
