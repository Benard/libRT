//
// Created by droid on 10/18/17.
//

#include <stdexcept>
#include <iostream>
#include "rt/Char.hpp"
#include "rt/signal/Manager.hpp"
#include "rt/io/fileno.hpp"
#include "rt/io/isatty.hpp"
#include "rt/io/Shell.hpp"
#include "rt/signal/signal.hpp"


rt::io::Shell::Shell(std::initializer_list<rt::io::Shell::CallBack> call_backs):
    _cmds(),
    _prompt("> "),
    _stdin_is_tty(rt::io::isatty(rt::io::fileno(stdin))),
    _token(signal::handle(signal::Code::SIG_INT, [this]() {
        std::cout << std::endl;
        printPrompt();
    }))
{
    // Set commands
    addCommand("exit", [](ArgList const &) { return ExitStatus::EXIT; });
    for (auto const &call_back : call_backs) {
        _cmds.insert(std::make_pair(call_back.name, call_back));
    }
}

void rt::io::Shell::run() {
    std::string line;

    printPrompt();
    while (getline(std::cin, line)) {
        try {
            if (ExitStatus::EXIT == parseAndExec(line))
                return;
        }
        catch (std::exception const &e) {
            std::cerr << "Error: " << e.what() << std::endl;
        }
        printPrompt();
    }
    if (_stdin_is_tty)
        std::cout << "exit" << std::endl;
}

rt::io::Shell::ExitStatus   rt::io::Shell::parseAndExec(std::string const &line) {
    rt::ArgList argv = splitArgs(line);

    if (!argv.empty()) {
        auto fct_it = _cmds.find(argv[0]);

        if (fct_it == _cmds.end())
            throw std::runtime_error("Unknown command: \"" + argv[0] + "\"");
        return fct_it->second.call_back(argv);
    }
    return ExitStatus::CONTINUE;
}

std::string rt::io::Shell::getArg(std::string const &line, std::string::const_iterator &it) {
    auto it2 = it;

    while (it2 != line.cend() && !::isspace(*it2))
        ++it2;

    std::string arg(it, it2);
    it = it2;
    return arg;
}

std::string     rt::io::Shell::getInhibArg(std::string const &line, std::string::const_iterator &it) {
    char inhibitor = *it;
    auto it2 = ++it;

    while (line.cend() != it2 && inhibitor != *it2)
        ++it2;
    if (line.cend() == it2)
        throw std::runtime_error("Missing inhibitor");

    std::string arg(it, it2);
    it = it2 + 1;
    return arg;
}

rt::ArgList     rt::io::Shell::splitArgs(std::string const &line) {
    rt::ArgList argv;

    auto it = line.cbegin();
    while (line.cend() != it) {
        // Pass all spaces
        while (Char::isspace(*it)) {
            ++it;
        }
        if (line.cend() == it)
            break;

        std::string arg = Char::isinhibitor(*it) ? getInhibArg(line, it) : getArg(line, it);
        argv.emplace_back(std::move(arg));
    }
    return argv;
}

void rt::io::Shell::printPrompt() const {
    if (_stdin_is_tty) {
        std::cout << _prompt;
        std::cout.flush();
    }
}
