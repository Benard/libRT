//
// Created by droid on 11/2/17.
//

#include <cstring>
#include "rt/platform.hpp"
#include "rt/strerror.hpp"

#if defined(RT_WIN_OS)

static const std::size_t    BUFF_SIZE = 256;

std::string rt::strerror(int err_num) {
    char    buff[BUFF_SIZE];

    ::strerror_s(buff, BUFF_SIZE, err_num);
    return std::string(buff);
}

#else

std::string rt::strerror(int err_num) {
    return ::strerror(err_num);
}

#endif
