//
// Created by droid on 11/2/17.
//

#pragma once

#include <cerrno>
#include <string>

namespace   rt {
    std::string     strerror(int err);
}
