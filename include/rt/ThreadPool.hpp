//
// Created by benard_g on 10/09/2017.
//

#pragma once

#include <cstddef>
#include <memory>
#include <functional>

namespace   rt {
    /*!
     * @author  G. Benard
     * @file    ThreadPool.hpp
     * @class   ThreadPool
     * @brief   Provides a pool of threads that can be used to execute tasks
     */
    class   ThreadPool {
    public:
        /*!
         * @brief   Initialize the TreadPool with the requested number of threads
         *
         * @param   nb_thread   The number of threads the pool will contain
         */
        explicit ThreadPool(std::size_t nb_thread);
        ~ThreadPool() noexcept;

        // Can be moved
        ThreadPool(ThreadPool&&) noexcept;
        ThreadPool &operator=(ThreadPool&&) noexcept;

        // Can't be copied
        ThreadPool(ThreadPool const &) = delete;
        ThreadPool &operator=(ThreadPool const &) = delete;

    public:
        /*!
         * @brief   Add a function to the task list. This function will be executed once a thread is available
         *
         * @param   task    A function task that will be added in the queue
         */
        void    addTask(std::function<void()> const &task);

    public:
        /*!
         * @return  The total number of threads
         */
        std::size_t     nbThreads() const noexcept;

        /*!
         * @return  The number of working threads
         */
        std::size_t     nbWorkingThreads() const noexcept;

        /*!
         * @return  The number of available threads
         */
        std::size_t     nbAvailableThreads() const noexcept;

    public:
        /*!
         * @brief   Wait for all threads to finish their execution.
         * @note    The pool can't be used anymore once this function is called
         */
        void    join() noexcept;

    private:
        /*
         * Using pimpl idiom
         */
        struct  Impl;
        std::unique_ptr<Impl>   _pimpl;
    };
}
