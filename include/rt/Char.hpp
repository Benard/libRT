//
// Created by droid on 10/21/17.
//

#pragma once

#include <cctype>
#include <iostream>

namespace   rt {
    /*!
     * @class   Char
     * @author  G. Benard
     * @file    Char.hpp
     * @brief   An utility class for char object
     */
    class   Char {
    private:
        char    _c;

    public:
        /*!
         * @brief   Constructs Char from system builtin char
         */
        Char(char c): _c(c) {}

        ~Char() noexcept = default;

        // Can be copied
        Char(Char const &) = default;
        Char    &operator=(Char const &) = default;

        // Can be moved
        Char(Char&&) = default;
        Char    &operator=(Char&&) = default;

    public:
        operator char() const { return _c; }

    public:
        /*!
         * @brief   Checks for an alphabetic character
         */
        bool    isalpha() const { return ::isalpha(_c); }

        /*!
         * @brief   Checks for a digit (0 through 9)
         */
        bool    isdigit() const { return ::isdigit(_c); }

        /*!
         * @brief   Checks for an alphanumeric character;
         *          it is equivalent to (isalpha(c) || isdigit(c))
         */
        bool    isalnum() const { return ::isalnum(_c); }

        /*!
         * @brief   Checks for hexadecimal digits, that is, one of
         *          "0 1 2 3 4 5 6 7 8 9 a b c d e f A B C D E F"
         */
        bool    isxdigit() const { return ::isxdigit(_c); }

    public:
        /*!
         * @brief   Checks for a blank character; that is, a space or a tab.
         */
        bool    isblank() const { return ::isblank(_c); }

        /*!
         * @brief   Checks for white-space characters
         */
        bool    isspace() const { return ::isspace(_c); }

    public:
        /*!
         * @brief   Checks for any printable character except space
         */
        bool    isgraph() const { return ::isgraph(_c); }

        /*!
         * @brief   Checks for any printable character including space
         */
        bool    isprint() const { return ::isprint(_c); }

    public:
        /*!
         * @brief   Checks for a control character
         */
        bool    iscntrl() const { return ::iscntrl(_c); }

        /*!
         * @brief   Checks for any printable character which is not a space
         *          or an alphanumeric character
         */
        bool    ispunct() const { return ::ispunct(_c); }

        /*!
         * @brief   Check for an inhibitor character (simple or double quote)
         */
        bool    isinhibitor() const { return '"' == _c || '\'' == _c; }

    public:
        /*!
         * @brief   Checks for a lowercase character
         */
        bool    islower() const { return ::islower(_c); }

        /*!
         * @brief   Checks for an uppercase letter
         */
        bool    isupper() const { return ::isupper(_c); }

        /*!
         * @return  If c is a uppercase letter, returns its lowercase equivalent.
         *          Otherwise, it returns c.
         */
        Char    tolower() const { return static_cast<char>(::tolower(_c)); }

        /*!
         * @return  If c is a lowercase letter, returns its uppercase equivalent.
         *          Otherwise, it returns c.
         */
        Char    toupper() const { return static_cast<char>(::toupper(_c)); }



    public:
        /*!
         * @brief   Checks for an alphabetic character
         */
        static bool isalpha(Char c) { return c.isalpha(); }

        /*!
         * @brief   Checks for a digit (0 through 9)
         */
        static bool isdigit(Char c) { return c.isdigit(); }

        /*!
         * @brief   Checks for an alphanumeric character;
         *          it is equivalent to (isalpha(c) || isdigit(c))
         */
        static bool isalnum(Char c) { return c.isalnum(); }

        /*!
         * @brief   Checks for hexadecimal digits, that is, one of
         *          "0 1 2 3 4 5 6 7 8 9 a b c d e f A B C D E F"
         */
        static bool isxdigit(Char c) { return c.isxdigit(); }

    public:
        /*!
         * @brief   Checks for a blank character; that is, a space or a tab.
         */
        static bool isblank(Char c) { return c.isblank(); }

        /*!
         * @brief   Checks for white-space characters
         */
        static bool isspace(Char c) { return c.isspace(); }

    public:
        /*!
         * @brief   Checks for any printable character except space
         */
        static bool isgraph(Char c) { return c.isgraph(); }

        /*!
         * @brief   Checks for any printable character including space
         */
        static bool isprint(Char c) { return c.isprint(); }

    public:
        /*!
         * @brief   Checks for a control character
         */
        static bool iscntrl(Char c) { return c.iscntrl(); }

        /*!
         * @brief   Checks for any printable character which is not a space
         *          or an alphanumeric character
         */
        static bool ispunct(Char c) { return c.ispunct(); }

        /*!
         * @brief   Check for an inhibitor character (simple or double quote)
         */
        static bool isinhibitor(Char c) { return c.isinhibitor(); }

    public:
        /*!
         * @brief   Checks for a lowercase character
         */
        static bool islower(Char c) { return c.islower(); }

        /*!
         * @brief   Checks for an uppercase letter
         */
        static bool isupper(Char c) { return c.isupper(); }

        /*!
         * @return  If c is a uppercase letter, returns its lowercase equivalent.
         *          Otherwise, it returns c.
         */
        static Char tolower(Char c) { return c.tolower(); }

        /*!
         * @return  If c is a lowercase letter, returns its uppercase equivalent.
         *          Otherwise, it returns c.
         */
        static Char toupper(Char c) { return c.toupper(); }
    };
}
