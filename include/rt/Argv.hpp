//
// Created by droid on 10/21/17.
//

#pragma once

#include <string>
#include <vector>

namespace   rt {
    using Argv = char const * const *;

    using ArgList = std::vector<std::string>;
}
