//
// Created by benard_g on 10/09/2017.
//

#pragma once

#include <memory>

/*!
 * @file    Singleton.hpp
 * @brief   An implementation of the Singleton Design Pattern
 * @author  G. Benard
 */
namespace   rt {
    /*!
     * @class   Singleton
     * @tparam  T The class to make into a Singleton
     */
    template <typename T>
    class   Singleton {
    public:
        /*!
         * @return An instance of the Singleton object
         */
        static T    &get();
    };
}

/*
 * Implementation
 */
template<typename T>
T& rt::Singleton<T>::get() {
    static std::unique_ptr<T>   elem(std::make_unique<T>());
    return *elem;
}
