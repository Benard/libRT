//
// Created by droid on 10/18/17.
//

#pragma once

#include <string>
#include <map>
#include <functional>
#include "rt/Argv.hpp"
#include "rt/signal/Token.hpp"

namespace       rt {
    namespace   io {
        /*!
         * @author  G. Benard
         * @file    Shell.hpp
         * @class   Shell
         * @brief   Provide a command line interpreter that calls the given callbacks
         */
        class   Shell {
        public:
            enum class  ExitStatus  {
                CONTINUE,
                EXIT
            };

        public:
            struct  CallBack {
            public:
                using Fct = std::function<ExitStatus (rt::ArgList const &)>;

            public:
                std::string     name;
                Fct             call_back;
            };


        public:
            /*!
             * @brief   Initialize the Shell with the given call_backs.
             *          Each call_back is associated to one command.
             *
             * @param   call_backs  A list of CallBack objects
             */
            Shell(std::initializer_list<Shell::CallBack> call_backs);

            // Coplient form
            Shell() = default;
            ~Shell() noexcept = default;

            // Can't be copied
            Shell(Shell const &) = delete;
            Shell &operator=(Shell const &) = delete;

            // Can be moved
            Shell(Shell&&) noexcept = default;
            Shell &operator=(Shell&&) noexcept = default;

        public:
            /*!
             * @brief   Launch the command-line interpreter.
             */
            void    run();

        public:
            /*!
             * @brief   Change the aspect of the prompt
             */
            void    setPrompt(std::string const &new_prompt) { _prompt = new_prompt; }

        public:
            /*!
             * @brief   Add a new command in the list
             *
             * @param   cmd_name    The name of the new command
             * @param   cmd         The call_back to call when the command is encountered
             */
            void    addCommand(std::string const &cmd_name, Shell::CallBack::Fct const &cmd)
            { _cmds.insert(std::make_pair(cmd_name, Shell::CallBack{cmd_name, cmd})); }


        private:
            /*
             * Parse the line and execute the command if possible
             */
            ExitStatus  parseAndExec(std::string const &line);

            /*
             * Split the line into arguments
             */
            rt::ArgList splitArgs(std::string const &line);

            /*
             * Get the next argument (all characters until the next separator)
             */
            std::string getArg(std::string const &line, std::string::const_iterator &it);

            /*
             * Get the next argument, knowing we are between quotes
             * (all characters until the matching quote)
             */
            std::string getInhibArg(std::string const &line, std::string::const_iterator &it);

        private:
            /*
             * Print the prompt if stdin is a tty
             */
            void    printPrompt() const;


        private:
            std::map<std::string, CallBack>   _cmds;

        private:
            std::string     _prompt;
            bool            _stdin_is_tty;

        private:
            signal::Token   _token;
        };
    }
}
