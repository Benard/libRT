//
// Created by droid on 10/28/17.
//

#pragma once

#include <cstdio>
#include "rt/platform.hpp"

namespace       rt {
    namespace   io {
#ifdef RT_WIN_OS
        int     fileno(FILE *stream) { return ::_fileno(stream); }
#else
        int     fileno(FILE *stream) { return ::fileno(stream); }
#endif
    }
}
