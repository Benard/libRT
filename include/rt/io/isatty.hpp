//
// Created by droid on 10/22/17.
//

#pragma once

#ifdef RT_WIN_OS
    #include <io.h>
#else
    #include <unistd.h>
#endif

namespace       rt {
    namespace   io {
        #ifdef RT_WIN_OS
            bool    isatty(int fd) { return ::_isatty(fd) != 0; }
        #else
            bool    isatty(int fd) { return ::isatty(fd) != 0; }
        #endif
    }
}
