//
// Created by artoo on 11/5/17.
//

#pragma once

#include <cstdint>
#include <memory>

namespace       rt {
    namespace maths {
        /*!
         * @author  G. Benard
         * @file    RandomNumber.hpp
         * @class   RandomNumber
         *
         * @brief   A class that manage random numbers generation
         */
        class RandomNumber {
        public:
            /*!
             * @brief   Create a random number generator.
             *
             * @param   min_bound   The minimum value that can be generated
             * @param   max_bound   the maximum value that can be generated
             */
            explicit RandomNumber();
            RandomNumber(std::size_t min_bound, std::size_t max_bound);
            ~RandomNumber() noexcept;

            // Can be moved
            RandomNumber(RandomNumber &&) noexcept;
            RandomNumber &operator=(RandomNumber &&) noexcept;

            // Can't be copied
            RandomNumber(RandomNumber const &) = delete;
            RandomNumber &operator=(RandomNumber const &) = delete;

        public:
            /*!
             * @brief   Generate a new random number
             *
             * @return  The generated number
             */
            std::size_t rand();

        public:
            /*!
             * @brief   Change the boundaries of the random number generator
             *
             * @param   min_bound   The minimum value that can be generated
             * @param   max_bound   The maximum value that can be generated
             */
            void setBoundaries(std::size_t min_bound, std::size_t max_bound);

        public:
            /*!
             * @return  The value of min_bound
             */
            std::size_t min() const noexcept;

            /*!
             * @return  The value of max_bound
             */
            std::size_t max() const noexcept;

        private:
            // using pimpl idiom
            struct Impl;
            std::unique_ptr<Impl> _pimpl;
        };
    }
}
