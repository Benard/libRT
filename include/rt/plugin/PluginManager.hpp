//
// Created by artoo on 11/15/17.
//

#pragma once

#include <map>
#include <string>
#include <experimental/filesystem>
#include "rt/plugin/Plugin.hpp"
#include "rt/plugin/Library.hpp"

namespace       rt {
    namespace   plugin {
        /*!
         * @author      G. Benard
         * @class       PluginManager
         * @file        PluginManager.hpp
         *
         * @brief       Manage plugins loading operations
         *
         * @tparam      Plugin  The type of plugins that will be managed
         */
        template <typename PluginType>
        class   PluginManager {
        public:
            /*!
             * @brief   Load all plugins contained in directory_name
             *
             * @param   directory_name  The name of the directory to inspect
             *
             * @tparam  Args            Additional arguments that will be given to each
             *                          plugin when built
             */
            template <typename ...Args>
            void    loadFromDirectory(std::string const &directory_name, Args&& ...args);

        public:
            /*!
             * @brief   Load a plugin from a library file
             *
             * @param   file_name   The name of the file to load
             *
             * @tparam  Args        Additional arguments that will be forwarded to the plugin
             */
            template <typename ...Args>
            void    loadFromFile(std::string const &file_name, Args&& ...args);

        public:
            /*!
             * @brief   Remove all plugins
             */
            void    clear() { _plugins.clear(); }

        public:
            using PluginContainer = std::map<std::string, Plugin<PluginType>>;
            using PluginIterator = typename PluginContainer::iterator;
            using PluginConstIterator = typename PluginContainer::const_iterator;

        public:
            /*!
             * @return  Returns an iterator on the beginning of the loaded plugins
             */
            PluginIterator      begin() { return _plugins.begin(); }

            /*!
             * @return  Returns a const_iterator on the beginning of the loaded plugins
             */
            PluginConstIterator begin() const noexcept { return _plugins.cbegin(); }

            /*!
             * @return  Returns a const_iterator on the beginning of the loaded plugins
             */
            PluginConstIterator cbegin() const noexcept { return _plugins.cbegin(); }

        public:
            /*!
             * @return  Returns an iterator on the end of the loaded plugins
             */
            PluginIterator      end() { return _plugins.end(); }

            /*!
             * @return  Returns a const_iterator on the end of the loaded plugins
             */
            PluginConstIterator end() const noexcept { return _plugins.cend(); }

            /*!
             * @return  Returns a const_iterator on the end of the loaded plugins
             */
            PluginConstIterator cend() const noexcept { return _plugins.cend(); }

        public:
            PluginManager() = default;
            ~PluginManager() noexcept = default;

            // Can't be copied
            PluginManager(PluginManager const &) = delete;
            PluginManager &operator=(PluginManager const &) = delete;

            // Can be moved
            PluginManager(PluginManager&&) noexcept = default;
            PluginManager &operator=(PluginManager&&) noexcept = default;


        private:
            PluginContainer _plugins;
        };
    }
}

//
// Implementation
//
template <typename PluginType>
template <typename ...Args>
void rt::plugin::PluginManager<PluginType>::loadFromDirectory(std::string const &directory_name, Args&& ...args) {
    std::experimental::filesystem::directory_iterator   dir(directory_name);

    for (auto const &entry : dir) {
        try {
            auto const &file_path = entry.path();

            if (file_path.extension().string() == rt::plugin::Library::extension()) {
                rt::plugin::Plugin<PluginType>  plug(file_path.string(), args...);
                std::string plug_name = plug.name();

                _plugins.emplace(std::make_pair(std::move(plug_name), std::move(plug)));
            }
        }
        catch (std::exception const &) {
            // Ignored for now
        }
    }
}

template <typename PluginType>
template <typename ...Args>
void rt::plugin::PluginManager<PluginType>::loadFromFile(std::string const &file_name, Args&& ...args) {
    rt::plugin::Plugin<PluginType>  plug(file_name, std::forward<Args>(args)...);
    std::string plugin_name = plug.name();

    _plugins.emplace(std::make_pair(std::move(plugin_name), std::move(plug)));
}
