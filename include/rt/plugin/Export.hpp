//
// Created by droid on 10/11/17.
//

#pragma once

#include "rt/platform.hpp"

/*!
 * @file    export.hpp
 * @author  G. Benard
 *
 * @brief   Add this MACRO in the definition of the function you want to be able to load from a dll
 */
#ifdef RT_WIN_OS
    #define RT_DLL_EXPORT  extern "C" __declspec(dllexport)
#else
    #define RT_DLL_EXPORT  extern "C"
#endif
