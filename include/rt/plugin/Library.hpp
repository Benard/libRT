//
// Created by droid on 10/11/17.
//

#pragma once

#include <string>
#include <memory>
#include "rt/platform.hpp"

namespace       rt {
    namespace   plugin {
        /*!
         * @author  G. Benard
         * @file    Library.hpp
         * @brief   An utility to load a dll and access its symbols
         * @class   Library
         */
        class   Library {
        public:
            /*!
             * @brief   Loads a library
             * @param   library_pathname    The pathname of the library you want to load
             */
            explicit Library(std::string const &library_pathname);
            ~Library() noexcept;

            // Can be moved
            Library(Library&&) noexcept;
            Library &operator=(Library&&) noexcept;

            // Can't be copied
            Library(Library const &) = delete;
            Library &operator=(Library const &) = delete;

        public:
            /*!
             * @brief   Loads a symbol from the loaded Library
             *
             * @param   symbol_name     the name of the symbol you want to load
             * @return  The requested symbol
             */
            void    *loadSymbol(std::string const &symbol_name);

        public:
            /*!
             * @return  The name of the loaded library
             */
            std::string const   &name() const noexcept;

        public:
            /*!
             * @return  Returns the extension for library files on the current system
             */
            static std::string const        &extension() {
                #if defined(RT_LINUX_OS)
                    static std::string const    _ext(".so");
                #elif defined(RT_MAC_OS)
                    static std::string const    _ext(".dylib");
                #else
                    static std::string const    _ext(".dll");
                #endif
                return _ext;
            }

        private:
            /*
             * Using pimpl idiom
             */
            struct Impl;
            std::unique_ptr<Impl>   _pimpl;
        };
    }
}
