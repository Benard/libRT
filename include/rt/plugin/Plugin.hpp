//
// Created by artoo on 11/12/17.
//

#pragma once

#include <string>
#include <utility>
#include <memory>
#include <stdexcept>
#include <experimental/filesystem>
#include "rt/plugin/Library.hpp"

namespace       rt {
    namespace   plugin {
        /*!
         * @author  G. Benard
         * @file    Plugin.hpp
         * @class   Plugin
         *
         * @brief   Loads a library and retrieve a Plugin object
         *
         * @tparam  PluginType  The type of Plugin object to retrieve
         */
        template <typename PluginType>
        class   Plugin {
        public:
            /*!
             * @brief   Loads the plugin by retrieving its ctor_sym and dtor_sym
             *          and calls the ctor_sym to instantiate the PluginType
             *
             * @param   file_name   The name of the library file
             * @param   args        Additional args that will be forwarded to ctor_sym
             */
            template <typename ...Args>
            explicit Plugin(std::string const &file_name, Args&& ...args);

            Plugin() = delete;
            ~Plugin() noexcept = default;

            // Can't be copied
            Plugin(Plugin const &) = delete;
            Plugin &operator=(PluginType const &) = delete;

            // Can be moved
            Plugin(Plugin&& other) noexcept;
            Plugin &operator=(Plugin&& other) noexcept;

        public:
            /*!
             * @return  The actual PluginType instance
             */
            PluginType  &operator()() noexcept { return *_plugin; }

            /*!
             * @see     operator()()
             */
            PluginType const &operator()() const noexcept { return *_plugin; }

        public:
            /*!
             * @return  The name of the ctor_sym
             *          This symbol will be used to retrieve the PluginType instance
             */
            static std::string const    &ctor_sym() {
                static std::string      _ctor("plugin_create");
                return _ctor;
            }

            /*!
             * @return  The name of the dtor_sym
             *          This symbol will be used to delete the PluginType instance
             */
            static std::string const    &dtor_sym() {
                static std::string      _dtor("plugin_delete");
                return _dtor;
            }

        public:
            /*!
             * @return  The name of the loaded plugin
             */
            std::string const   &name() const noexcept { return _plugin_name; }

        public:
            /*!
             * @return  The Library object
             */
            Library const &library() const noexcept { return _library; }

        private:
            Library     _library;

        private:
            using PluginDeletor = void (*)(PluginType *);
            std::shared_ptr<PluginType> _plugin;

        private:
            std::string _plugin_name;
        };
   }
}

//
// Impl
//
template<typename PluginType>
template<typename ...Args>
rt::plugin::Plugin<PluginType>::Plugin(std::string const &file_name, Args&& ...args):
    _library(file_name),
    _plugin(nullptr),
    _plugin_name(std::experimental::filesystem::path(file_name).stem().string())
{
    // Load plugin
    auto ctor = reinterpret_cast<PluginType *(*)(Args&&...)>(_library.loadSymbol(ctor_sym()));
    auto dtor = reinterpret_cast<void (*)(PluginType *)>(_library.loadSymbol(dtor_sym()));

    _plugin.reset(ctor(std::forward<Args>(args)...), dtor);
    if (!_plugin)
        throw std::runtime_error("Could not load plugin object");

    // Remove "lib" prefix from plugin name
    if (0 == _plugin_name.compare(0, 3, "lib"))
        _plugin_name = _plugin_name.substr(3, std::string::npos);
}

template <typename PluginType>
rt::plugin::Plugin<PluginType>::Plugin(rt::plugin::Plugin<PluginType> &&other) noexcept:
    _library(std::move(other._library)),
    _plugin(std::move(other._plugin)),
    _plugin_name(std::move(other._plugin_name))
{}

template <typename PluginType>
rt::plugin::Plugin<PluginType> &rt::plugin::Plugin<PluginType>::operator=(rt::plugin::Plugin<PluginType> &&other) noexcept {
    std::swap(_library, other._library);
    std::swap(_plugin, other._plugin);
    std::swap(_plugin_name, other._plugin_name);
    return *this;
}
