//
// Created by droid on 10/22/17.
//

#pragma once

#include <unordered_map>
#include <set>
#include <functional>
#include "rt/Singleton.hpp"
#include "rt/signal/Code.hpp"


namespace       rt {
    namespace   signal {
        /*!
         * @author  G. Benard
         * @file    Manager.hpp
         * @class   Manager
         * @brief   A class that manage signal handlers
         */
        class   Manager: public Singleton<Manager> {
        public:
            using Handler = std::function<void (void)>;

        public:
            /*!
             * @brief   Set a new handler for a signal.
             * @note    Several handler can be at the set same time.
             *          Use reset if you want to remove the others.
             *
             * @param   signal      The code of the signal to handle
             * @param   handler     The handler to call whenever the signal is caught
             *
             * @return  Returns the handler_id, it can be used to call reset()
             *          (see rt::signal::Token to manage handler's life-time)
             */
            std::size_t handle(signal::Code signal_code, Handler handler);

        public:
            /*!
             * @brief   Clear all handlers for a given signal and ignore
             *          its future appearances
             *
             * @param   signal  The code of the signal to ignore
             */
            void ignore(signal::Code signal_code);

        public:
            /*!
             * @brief   Clear all handlers for a given signal and set it to its default behavior
             *
             * @param   signal  The code of the signal to reset
             */
            void reset(signal::Code signal_code);

            /*!
             * @brief   Remove an specific handler.
             *          If all handlers are removed for a given
             *          signal_code, the default behavior is applied
             *
             * @param   handler_id  The id returned by the call to handle,
             *          it represents the handler
             */
            void reset(std::size_t handler_id);

            /*!
             * @brief   Reset all signals to the default behavior
             */
            void reset();

        public:
            /*!
             * @brief   Checks whether a signal_code is handled or not
             *
             * @param   signal_code     The signal code to be check
             *
             * @return  Returns true if the signal_code is handled, false otherwise
             */
            bool isHandled(rt::signal::Code signal_code) const noexcept;

        public:
            /*!
             * @brief   Checks whether a signal_code is ignored or not
             *
             * @param   signal_code     The signal code to verify
             *
             * @return  Returns true if the signal_code is ignored, false otherwise
             */
            bool isIgnored(rt::signal::Code signal_code) const noexcept;


        public:
            // Coplient form
            Manager();
            ~Manager() noexcept = default;

            // Can;t be copied
            Manager(Manager const &) = delete;
            Manager &operator=(Manager const &) = delete;

            // Can't be moved
            Manager(Manager&&) = delete;
            Manager &operator=(Manager&&) = delete;


        private:
            /*
             * This function is set to be the sig_handler for the requested signal.
             * It will call all callbacks set to the signal code `sig_value`
             */
            static void     sigHandler(int sig_value);

        private:
            /*
             * Assign an handler using the signal function
             * Throw an exception if the call fails
             */
            void    assignHandler(signal::Code signal_code, void (*handler)(int));

        private:
            /*
             * Remove a code from the ignored list if possible
             */
            void    removeFromIgnored(signal::Code signal_code);

            /*
             * Remove all handler for the given signal_code
             */
            void    removeFromHandled(signal::Code signal_code);


        private:
            using CallBackContainer = std::unordered_map<std::size_t, std::pair<signal::Code, Handler>>;

            std::size_t         _id;
            CallBackContainer   _call_backs;

        private:
            std::set<signal::Code>  _ignored_codes;
        };
    }
}
