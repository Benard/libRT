//
// Created by droid on 10/22/17.
//

#pragma once

#include <csignal>
#include "rt/platform.hpp"

namespace       rt {
    namespace   signal {
        /*!
         * @author  G. Benard
         * @file    Code.hpp
         */
        enum class  Code {
            UNSUPPORTED = -1,

            #ifdef RT_WIN_OS
                SIG_INT  = SIGINT,
                SIG_ILL  = SIGILL,
                SIG_ABRT = SIGABRT,
                SIG_FPE  = SIGFPE,
                SIG_SEGV = SIGSEGV,
                SIG_TERM = SIGTERM,

                // The following signals are not handled by Windows systems
                SIG_HUP  = static_cast<int>(UNSUPPORTED),

                SIG_QUIT = static_cast<int>(UNSUPPORTED),
                SIG_KILL = static_cast<int>(UNSUPPORTED),
                SIG_PIPE = static_cast<int>(UNSUPPORTED),

                SIG_ALRM = static_cast<int>(UNSUPPORTED),
                SIG_USR1 = static_cast<int>(UNSUPPORTED),
                SIG_USR2 = static_cast<int>(UNSUPPORTED),
                SIG_CHLD = static_cast<int>(UNSUPPORTED),
                SIG_CONT = static_cast<int>(UNSUPPORTED),
                SIG_STOP = static_cast<int>(UNSUPPORTED),
                SIG_TSTP = static_cast<int>(UNSUPPORTED),
                SIG_TTIN = static_cast<int>(UNSUPPORTED),
                SIG_TTOU = static_cast<int>(UNSUPPORTED)
            #else
                SIG_HUP  = SIGHUP,

                SIG_INT  = SIGINT,
                SIG_QUIT = SIGQUIT,
                SIG_ILL  = SIGILL,
                SIG_ABRT = SIGABRT,
                SIG_FPE  = SIGFPE,
                SIG_KILL = SIGKILL,
                SIG_SEGV = SIGSEGV,
                SIG_PIPE = SIGPIPE,

                SIG_ALRM = SIGALRM,
                SIG_TERM = SIGTERM,
                SIG_USR1 = SIGUSR1,
                SIG_USR2 = SIGUSR2,
                SIG_CHLD = SIGCHLD,
                SIG_CONT = SIGCONT,
                SIG_STOP = SIGSTOP,
                SIG_TSTP = SIGTSTP,
                SIG_TTIN = SIGTTIN,
                SIG_TTOU = SIGTTOU
            #endif
        };
    }
}
