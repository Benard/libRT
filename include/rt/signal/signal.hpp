//
// Created by droid on 10/24/17.
//

#pragma once

#include "rt/signal/Code.hpp"
#include "rt/signal/Token.hpp"
#include "rt/signal/Manager.hpp"

namespace       rt {
    namespace   signal {
        /*!
         * @brief   Register a handler for a signal_code and return
         *
         * @param   signal_code     The signal to handle
         * @param   handler         The function the will be called when the signal id catched
         *
         * @return  Returns a signal::Token managing the life-time of the signal
         */
        Token   handle(signal::Code signal_code, Manager::Handler handler);
    }
}
