//
// Created by droid on 10/23/17.
//

#pragma once

#include <cstddef>
#include <algorithm>
#include "rt/signal/Manager.hpp"

namespace       rt {
    namespace   signal {
        /*!
         * @author  G. Benard
         * @file    Token.hpp
         * @class   Token
         *
         * @brief   Manages the life-time of a signal handler.
         *          The class automatically calls signal::Manager::reset when it is destroyed.
         *
         *          You can see this class as an equivalent of std::unique_ptr
         *          for signal handlers.
         */
        class   Token {
        public:
            /*!
             * @param   handler_id  The id returned by signal::Manager::handle
             */
            explicit Token(std::size_t handler_id = 0): _handler_id(handler_id) {}

            /*!
             * @brief   When the destructor is called, the signal handler is also disabled
             */
            ~Token() noexcept {
                try {
                    if (_handler_id != 0) signal::Manager::get().reset(_handler_id);
                }
                catch (...) {}
            }

        public:
            /*
             * Can be moved
             */
            Token(Token&& other) noexcept : _handler_id(other._handler_id)
            { other._handler_id = 0; }

            Token &operator=(Token&& other) noexcept {
                std::swap(_handler_id, other._handler_id);
                return *this;
            }

        public:
            /*
             * Can't be copied
             */
            Token(Token const &) = delete;
            Token &operator=(Token const &) = delete;

        private:
            std::size_t _handler_id;
        };
    }
}
