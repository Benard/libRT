//
// Created by droid on 10/29/17.
//

#pragma once

/*!
 * @file    platform.h
 * @author  G. Benard
 *
 * @brief   Defines a set of macros providing an uniform way
 *          to identify the operating system
 */
#if defined(_WIN32)
    // Windows system
    #define RT_WIN_OS

    #if defined(_WIN64)
        // Windows 64 bits system
        #define RT_WIN_64
    #else
        // Windows 32 bits system
        #define RT_WIN_32
    #endif
#endif

#if defined(__unix) || defined(__unix__)
    // Unix system, should work for both mac and linux
    #define RT_UNIX_OS
#endif

#if defined(__linux__)
    // Linux system
    #define RT_LINUX_OS
#endif

#if defined(__APPLE__)
    // MacOS system
    #define RT_MAC_OS
#endif
